BANDIT_FLAGS := -r -ll -ii
MYPY_FLAGS := --ignore-missing-imports --strict-optional
PYVERSION := py38
REGISTRY := registry.gitlab.com/mtrsk/trek
SHELL := bash
SOURCE := trek/ tests/
PROJECT := trek


build-dev:
	@DOCKER_BUILDKIT=1 docker image build \
		--target=dev-image \
		-t $(REGISTRY) .


build-prod:
	@DOCKER_BUILDKIT=1 docker image build \
		--target=prod-image \
		-t $(REGISTRY) .


format:
	isort --recursive $(SOURCE)
	black --target-version $(PYVERSION) $(SOURCE)


mypy:
	mypy $(MYPY_FLAGS) $(SOURCE)


test:
	pytest tests --cov="$(PROJECT)"


run:
	@docker-compose up -d --build

