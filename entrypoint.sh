#!/usr/bin/env bash

set -ex

HOST=0.0.0.0
PORT=8000
WORKERS=1

poetry run \
    uvicorn trek:app \
        --reload \
        --workers "$WORKERS" \
        --host "$HOST" \
        --port "$PORT"
