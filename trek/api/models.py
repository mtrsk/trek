from pydantic import BaseModel


class Officer(BaseModel):
    name: str
    rank: str
