"""
This modules handles different configuration environments for the whole
project.
"""

import os
from dataclasses import dataclass
from pathlib import Path

from dotenv import load_dotenv

# Get the absolute path for this __file__, check it's parent (..)
# and load the .env file that could be present at the project's
# root directory.
env_path: Path = Path(__file__).parent.absolute() / Path(".env")
load_dotenv(dotenv_path=env_path)

PROJECT_NAME = "trek"
DEFAULT_SETTINGS: str = f"{PROJECT_NAME}.configuration.Production"


@dataclass
class Base:
    DEBUG: bool = False
    TESTING: bool = False
    DATABASE_URL: str = os.getenv("DATABASE_URL", "")


@dataclass
class Development(Base):
    DEBUG: bool = True


@dataclass
class Production(Base):
    pass


@dataclass
class Testing(Base):
    TESTING: bool = True
