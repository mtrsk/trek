# ============================================
# Following Docker's best practices defined in
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
# ============================================
FROM python:3.8.1-slim-buster as base

# It seems poetry is not modifying the PATH and it has to be done manually
ENV PATH="/home/app/.poetry/bin:$PATH"
ENV POETRY_VERSION="1.0.2"

# Install dependencies.
RUN apt-get update --fix-missing \
    && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        libpq-dev \
        libssl-dev \
    && rm -rf /var/lib/apt/lists/*

# Create an unprivileged user to run inside the container
# then define a workdir for such user.
RUN useradd --create-home --user-group app
WORKDIR /home/app

# Switch to the non-root user
USER app

# Install a pinned version of poetry
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -sSL \
    https://raw.githubusercontent.com/python-poetry/poetry/${POETRY_VERSION}/get-poetry.py | python

# Make sure to install dependencies in a .venv directory
RUN poetry config virtualenvs.in-project true

# Copy the setup (pyproject) and pinned dependencies
# (poetry.lock) and make them cached in the Docker
# layer.
#
# Remark: COPY with more than two arguments requires
# the last one to end with '/'.
COPY --chown=app:app pyproject.toml poetry.lock ./

# Install python dependencies
# FLAGS:
#   --no-interaction: do not to ask any interactive questions
#   --no-ansi: make output more log-friendly
#   --no-dev: no development dependencies
#   --no-root: do not install the current project
RUN poetry install \
    --no-interaction \
    --no-ansi \
    --no-dev \
    --no-root

# ===================================================
# Production Image
# ===================================================
FROM base as prod-image

# These variables are needed if you want to test the
# app with the Flask CLI via `flask run`.
ENV APP_SETTINGS="trek.configuration.Production"

# Copy the remaining source code, just make sure you have a .dockerignore
# in the projects root.
COPY --chown=app:app \
    entrypoint.sh \
    Makefile \
    ./
# There is no need to copy test files to production
COPY --chown=app:app trek /home/app/trek

RUN chmod 500 entrypoint.sh

# Container entrypoint
ENTRYPOINT [ "./entrypoint.sh" ]

# ===================================================
# CI Image
# ===================================================
FROM base as ci-image

# Same variables from production stage, but with
# the correct values set.
ENV APP_SETTINGS="trek.configuration.Testing"

# Install python dependencies
RUN poetry install --no-interaction --no-ansi

# Copy the remaining source code, just make sure you have a .dockerignore
# in the projects root.
COPY --chown=app:app \
    pytest.ini \
    entrypoint.sh \
    Makefile \
    ./
COPY --chown=app:app trek /home/app/trek
COPY --chown=app:app tests /home/app/tests

RUN chmod 500 entrypoint.sh

# ===================================================
# Dev Image
# ===================================================
FROM ci-image as dev-image

# Remark: Environment variables need to be redeclared in
# multi-stage builds.
ENV APP_SETTINGS="trek.configuration.Development"

# Extra volumes to expose poetry and the venv inside the container
# for developing purposes.
VOLUME [ "/home/app/.poetry", "/home/app/.venv" ]
