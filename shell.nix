{ bootstrap ? import <nixpkgs> {}
, json ? ./nix/.nixpkgs-version.json
, nixpkgs ? builtins.fromJSON (builtins.readFile json)
, pkgs ? import (bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs-channels";
    inherit (nixpkgs) rev sha256;
  }) {}
}:

let
  env = pkgs.poetry2nix.mkPoetryEnv {
    python = pkgs.python38;
    poetrylock = ./poetry.lock;
  };
in
  pkgs.mkShell {
    # poetry2nix still doesn't work
    buildInputs = with pkgs; [
      python38Full
      poetry
    ];
    shellHook = ''
      mkdir -p -- "$(pwd)/.cache/pypoetry"
      poetry config cache-dir $(pwd)/.cache/pypoetry --local
      poetry config virtualenvs.in-project true --local
    '';
  }
